package com.jiangfeixiang.sendemail;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class SendemailApplicationTests {

    /**
     * 注入发送邮件的接口
     */
    @Autowired
    private IMailService mailService;

    @Autowired
    private AgentToolConfiguration agentToolConfiguration;

    /**
     * 测试发送文本邮件
     */
    @Test
    public void sendmail() {
        String subject = String.format(agentToolConfiguration.getSubject(), "PMTOAM", "13566668888");
        String content = String.format(agentToolConfiguration.getContent(), "1", "2", "3", "4", "5");
        log.info("{}, {}, {}", agentToolConfiguration.getTo(), subject, content);
        mailService.sendSimpleMail(agentToolConfiguration.getTo(), subject, content);
    }

    @Test
    public void sendmailHtml(){
        mailService.sendHtmlMail("pmtoam@163.com","主题：你好html邮件","<h1>内容：第一封html邮件</h1>");
    }
}

