package com.jiangfeixiang.sendemail;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @author yuexing.wang
 */
@Data
@Component
@ConfigurationProperties
public class AgentToolConfiguration {

    @Value("${mail.to}")
    private String to;

    @Value("${mail.subject}")
    private String subject;

    @Value("${mail.content}")
    private String content;

}
